import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Header from './components/Header';
import Footer from './components/Footer';
import Cards from './components/Cards';
import DealsBySs from './pages/DealsBySs';
import DealsByRakesh from './pages/DealsByRakesh';
import SingleProduct from './components/SingleProduct';

function App() {
  const [searchTerm, setSearchTerm] = useState("");
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false)

  const handleSearch = (event) => { setSearchTerm(event.target.value); }


  // const fetchProduct = async () => {
  //   const res = await fetch('https://lootersisland.com/deal/deals-api.php?store=amazon&page=2');
  //   const data = await res.json()
  //   setProducts(data)
  //   setLoading(true)
  // }
  // useEffect(() => {
  //   fetchProduct()
  // }, [])


  useEffect(() => {
    setLoading(true)
    fetch('https://lootersisland.com/deal/deals-api.php?store=amazon&page=2')
    .then(response => response.json())
    .then(data => {
        setProducts(data)
        setLoading(false);
      })
      .catch(error => {
        console.error(error);
        setLoading(false);
      });
  }, []);



  return (
    <>
      <Router>
        <Header handleSearch={handleSearch} />
        <Routes>
          <Route path='/' element={<Cards products={products} searchTerm={searchTerm} loading={loading}  />} />
          <Route path='/cart/itemdetail/:id' element={<SingleProduct products={products}  />} />
          <Route path='/dealsbyss' element={<DealsBySs />} />
          <Route path='/dealsbyrakesh' element={<DealsByRakesh />} />
        </Routes>
        <Footer />
      </Router>
    </>
  );
}

export default App;
