import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { FaSearch } from 'react-icons/fa';
import '../style/Header.css'
import { HashLink } from "react-router-hash-link"

const Header = ({ handleSearch }) => {

 


    return (
        <div className='header-container'>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <HashLink className="navbar-brand" to="#">
                    <h1 className='logo'>
                        <HashLink to={"/"}>Lootersisland</HashLink>
                    </h1>
                </HashLink>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item dropdown">
                            <HashLink className="nav-link dropdown-toggle" to="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{ color: "#ffffff" }}>
                                Dealsby
                            </HashLink>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <HashLink className="dropdown-item" to={"/dealsbyss"}>DealsBySs</HashLink>
                                <HashLink className="dropdown-item" to={"/dealsbyrakesh"}>DealsByRakesh</HashLink>
                            </div>
                        </li>
                        <li className="nav-item">
                            <HashLink className="nav-link" to="#" style={{ color: "#ffffff" }}>Udemy Course</HashLink>
                        </li>
                        <li className="nav-item dropdown">
                            <HashLink className="nav-link dropdown-toggle" to="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{ color: "#ffffff" }}>
                                Food Order
                            </HashLink>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <HashLink className="dropdown-item" to="#">Zomato</HashLink>
                                <HashLink className="dropdown-item" to="#">Swiggy</HashLink>
                            </div>
                        </li>
                        <li className="nav-item dropdown">
                            <HashLink className="nav-link dropdown-toggle" to="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{ color: "#ffffff" }}>
                                Cab Service
                            </HashLink>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <HashLink className="dropdown-item" to="#">Ola Cabs</HashLink>
                                <HashLink className="dropdown-item" to="#">Uber India</HashLink>
                            </div>
                        </li>
                        <li className="nav-item dropdown">
                            <HashLink className="nav-link dropdown-toggle" to="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{ color: "#ffffff" }}>
                                Payment Apps
                            </HashLink>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <HashLink className="dropdown-item" to="#">Amazon Pay</HashLink>
                                <HashLink className="dropdown-item" to="#">Paytm</HashLink>
                                <HashLink className="dropdown-item" to="#">PhonePe</HashLink>
                                <HashLink className="dropdown-item" to="#">Mobikwik</HashLink>
                            </div>
                        </li>
                    </ul>
                    <form className="search-for-item" style={{ display: "flex" }}>
                        <div className='search'><input className="searchbar"
                            type="search" placeholder="Search. . ."
                            aria-label="Search" name='searchTerm' onChange={handleSearch} />
                        </div><FaSearch className='search-icon' />
                    </form>
                </div>
            </nav>
        </div>
    )
}

export default Header;