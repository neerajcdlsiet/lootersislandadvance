import React from 'react'
import { useParams } from 'react-router-dom'
import '../style/SingleProduct.css'
import Loader from './Loader'


const SingleProduct = ({ products, loading }) => {
  const { id } = useParams();
  const product = products.filter(g => g.id === id)[0];
  console.log("route111111", product)
  return (

    <div className="singleproduct-container">
       {loading ? <Loader /> :
      <div className='single-pro-wrapper'>
        <div className='img-part'>
          <img src={"https://lootersisland.com/deal/" + product.image} />
          <h6>Posted at <span>{product.date}</span></h6>
        </div>
        <div className='desc-part'>
          <h4>{product.name}</h4>
          <div className='price-section'>
            <p>Price: <span>₹{product.offer_price}</span></p>
            <p className='mrp'>MRP: <s>₹{product.actual_price}</s></p>
            <h5>{Math.ceil(((product.actual_price - product.offer_price) * 100) / product.actual_price)}% Off</h5>
          </div>
          <button className='get-deal-btn' onClick={() => {
            window.open(`${product.url}`, "_blank");
          }}>Get Deal</button>
        </div>
      </div> 
}
      <div className='get-deal-desc'>
        <h1>How to get this deal:-</h1>
        <ul>
          <li>Click On <button onClick={() => {
            window.open(`${product.url}`, "_blank");
          }}>Get Deal</button> Button to go to offer page</li>
          <li>Check for latest price & stock availability.</li>
          <li>Add to cart & select/update shipping info.</li>
          <li>Make payment.</li>
        </ul>
      </div>
      <form>
        <div className="form-group purple-border">
          <label htmlFor="exampleFormControlTextarea4">Comments</label>
          <textarea className="form-control" id="exampleFormControlTextarea4" rows="3"></textarea>
        </div>
        <button className='form-submit-btn'>Submit</button>
      </form>
    </div>
  )
}

export default SingleProduct