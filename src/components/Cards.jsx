import React, { useState } from 'react';
import '../style/Cards.css'
import { MdQueryBuilder } from "react-icons/md";
import { Link } from 'react-router-dom';
import Loader from './Loader';

const Cards = ({ products, searchTerm,loading }) => {
    const [moreDeal, setMoreDeal] = useState(10)

    const handleMoreDeal = () =>{
        setMoreDeal(moreDeal + 10)
    }

    return (
        <div className='cards-section'>
            <div className='my-card-container'>
                {loading ?<Loader /> : products.filter((val) => {
                    if (searchTerm === "") {
                        return val
                    } else if ((val.offer_price.toLowerCase().includes(searchTerm.toLowerCase())) || (val.name.toLowerCase().includes(searchTerm.toLowerCase()))) {
                        return val
                    }
                }).map(item => (
                    <div className='my-cards' key={item.id}>
                        <p><span><MdQueryBuilder /></span> {item.time}</p>
                        <Link to={{ pathname: `/cart/itemdetail/` + item.id }} >
                            <img src={"https://lootersisland.com/deal/" + item.image} alt="image not found" />
                        </Link>
                        <h6 className='desc' >{item.name.substr(0, 60)}</h6>
                        <div className='off-and-price'>
                            <h5>{Math.ceil(((item.actual_price - item.offer_price) * 100) / item.actual_price)}%</h5>
                            <div className='money-and-show-discount'>
                                <h5>₹ {item.offer_price}</h5>
                                <span><s>{item.actual_price}</s></span>
                            </div>
                        </div>
                        <div className='dealBy'>
                            <h6>{item.logo}</h6>
                            <button onClick={() => {
                                window.open(`${item.url}`);
                            }}>Get Deal</button>
                        </div>
                    </div>
                )).slice(0, moreDeal) 
            }
            </div>
            <div className='more-item'>
                <button onClick={handleMoreDeal}>More Deals</button>
            </div>
        </div>
    )
}

export default Cards;





