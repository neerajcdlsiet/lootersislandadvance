import React from 'react'
import "../style/Footer.css"
import { FaHome } from 'react-icons/fa';
const Footer = () => {
  return (
    <div className='footer-container'>
      <div className='footer-link-part'>
        <div>
          <h2>QUICK LINKS</h2>
          <div className='all-items-link'>
            <div className='items'>
              <a>Earphones</a>
              <a>Stickers</a>
            </div>
            <div className='items'>
              <a>Helmet</a>
              <a>Geyser</a>
            </div>
            <div className='items'>
              <a>TV</a>
              <a>Speaker</a>
            </div>
            <div className='items'>
              <a>Soap</a>
              <a>Shampoo</a>
            </div>
            <div className='items'>
              <a>Mask</a>
              <a>Sanitizer</a>
            </div>
            <div className='items'>
              <a>Oils</a>
              <a>Peanut</a>
            </div>
            <div className='items'>
              <a>Bags</a>
              <a>Luggages</a>
            </div>
            <div className='items'>
              <a>Purifier</a>
              <a>Kitchen items</a>
            </div>
            <div className='items-combine'>
              <a>Cable &nbsp; &nbsp; & &nbsp; &nbsp; Charger</a>
            </div>
            <div className='items-combine'>
              <a>Drives, &nbsp; Mobile &nbsp; & &nbsp; Laptop</a>
            </div>
          </div>
        </div>
        <div className='oursite-policy'>
          <ul>
            <li><a>About Us</a></li>
            <li><a>Terms and Conditions</a></li>
            <li><a>Privacy Policy</a></li>
            <li><a>Disclamer</a></li>
            <li><a>Contact Us</a></li>
          </ul>
        </div>
        <div className='address-section'>
          <h2>ADDRESS</h2>
          <div className='address'>
            <span><FaHome /></span>
            <p>New Delhi</p>
          </div>
        </div>
      </div>
      <div className='disclosure'>
        <h5>Disclosure : lootersisland is a participant in the Amazon Services LLC Associates Program,
          an affiliate advertising program designed to provide a means for sites to earn advertising
          fees by advertising and linking to Amazon.in.
        </h5>
        <p className='terms-privacy'>By Using This Site, you Agree to the <a>Terms of Service</a> and <a>Privacy Policy</a> of lootersisland</p>
        <span>© 2020</span>
        <h6>lootersisland.com. All rights reserved. All content, trademarks and logos are copyright of their respective owners.</h6>
      </div>
    </div>
  )
}

export default Footer