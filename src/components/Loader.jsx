import React from 'react'
import '../style/Loader.css'
const Loader = () => {
    return (
        <div className='loader-container'>
            <div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    )
}

export default Loader;